#!/usr/bin/env python
import sys
import getpass
import os.path
import anki
import anki.schedv2
import anki.sync
import anki.importing

root = '.local/share/anki-lcli'
colfilename = 'collection.anki2'
keyfilename = 'key'

def getdir():
    home = os.path.expanduser('~')
    d = os.path.join(home, root)
    os.makedirs(d, exist_ok=True)
    return d

def openCol():
    d = getdir()
    col = anki.Collection(os.path.join(d, colfilename))
    return col

def quit(col):
    print("Завершуємо...")
    col.close()
    sys.exit(0)

def inputOrQuit(col):
    try:
        l = input()
        if l == 'q':
            quit(col)
    except(KeyboardInterrupt):
        quit(col)
    return l

def learn(col):
    variants=range(1,5)
    print(col.stats().todayStats())
    s = anki.schedv2.Scheduler(col)
    while True:
        card = s.getCard()
        if not card:
            break
        n = card.note()
        print(n.values()[0])
        inputOrQuit(col)
        print(n.values()[1])
        for v in variants:
            print("{}: {}".format(v, s.nextIvlStr(card, v).replace('\u2068', '').replace('\u2069', '')))
        while True:
            try:
                v = int(inputOrQuit(col))
            except(ValueError):
                continue
            if v in variants:
                break
        s.answerCard(card, v)
    print("Усе вивчено!")

def login():
    user = input('Користувач anki: ')
    password = getpass.getpass('Пароль: ')
    server = anki.sync.RemoteServer(user, 2)
    hkey = server.hostKey(user, password)
    if not hkey:
        print("Не вдалося авторизуватися")
        sys.exit(1)
    d = getdir()
    with open(os.path.join(d, keyfilename), 'w') as f:
        f.write(hkey)
    print("Успішно записано ключ!")

def getkey():
    d = getdir()
    try:
        with open(os.path.join(d, keyfilename), 'r') as f:
            return f.read().strip()
    except FileNotFoundError:
        return None

def sync(col):
    hkey = getkey()
    if not hkey:
        print("Спочатку треба виконати команду login")
        return False
    server = anki.sync.RemoteServer(hkey, 2)
    client = anki.sync.Syncer(col, server)
    t = client.sync()
    if t == 'noChanges':
        print('Усе актуально')
    elif t == 'success':
        print('Успішна синхронізація')
    elif t == 'fullSync':
        print('Зміни схеми. Варто зробити резервну копію та спробувати команду fullsync')
    elif t == 'badAuth':
        print('Помилка авторизації. Якщо змінився пароль, спробуйте команду login')
    else:
        print('anki повернув невідомий результат: ' + t)

def fullsync(col):
    hkey = getkey()
    if not hkey:
        print("Спочатку треба виконати команду login")
        return False
    server = anki.sync.RemoteServer(hkey, 2)
    fullsyncer = anki.sync.FullSyncer(col, hkey, server.client, 2)
    fullsyncer.download()

def selectdeck(col):
    deckToId = {}
    all = col.decks.all()
    active = col.decks.active()[0]
    k = 0
    for d in all:
        k = k + 1
        deckToId[str(k)] = d
        if d['id'] == active:
            actstr = '* ' 
        else:
            actstr = ''
        print("{}{}: {}".format(actstr, k, d['name']))
    while True:
        v = input('Вибрати колоду: ')
        if v in deckToId:
            break
        print('Введіть номер колоди.')
    col.decks.select(deckToId[v]['id'])
    print("Колоду '{}' вибрано".format(deckToId[v]['name']))

def importfile(col, path):
    importer = anki.importing.csvfile.TextImporter(col, path)
    importer.initMapping()
    importer.run()
    if importer.log:
        print('Лоґ:')
        for l in importer.log:
            print(l)
    print('Імпорт завершено. Імпортовано: {}'.format(importer.total))

commands = ('login', 'learn', 'sync', 'fullsync', 'selectdeck', 'import')

def usage():
    print('Usage: anki-lcli.py ' + '|'.join(commands) + '|--help (default - learn)')

command = 'learn'
if len(sys.argv) > 1:
    command = sys.argv[1]

if command == '--help':
    usage()
    sys.exit(0)

if command not in commands:
    usage()
    sys.exit(1)

if command == 'learn':
    col = openCol()
    learn(col)
    quit(col)
elif command == 'sync':
    col = openCol()
    sync(col)
    quit(col)
elif command == 'fullsync':
    col = openCol()
    fullsync(col)
    quit(col)
elif command == 'login':
    login()
elif command == 'selectdeck':
    col = openCol()
    selectdeck(col)
    quit(col)
elif command == 'import':
    if len(sys.argv) < 3:
        print('Команда import потребує додаткового аргумента: шлях до файлу')
        sys.exit(1)
    path = sys.argv[2]
    col = openCol()
    importfile(col, path)
    quit(col)

